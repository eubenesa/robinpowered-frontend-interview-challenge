import moment from 'moment';
import findFreeTimes from './challenge';

const events = [
  {
    start: '2017-02-21T12:00:00-05:00',
    end: '2017-02-21T12:30:00-05:00'
  },
  {
    start: '2017-02-21T14:00:00-05:00',
    end: '2017-02-21T16:00:00-05:00'
  }
];

const start = moment('2017-02-21T08:00:00-05:00');
const end = moment('2017-02-21T18:00:00-05:00');

const freeTimes = findFreeTimes(start, end, 30, events);

test('Meeting Availability', () => {
  expect(freeTimes).toEqual([
    {
      start: '2017-02-21T08:00:00-05:00',
      end: '2017-02-21T12:00:00-05:00'
    },
    {
      start: '2017-02-21T12:30:00-05:00',
      end: '2017-02-21T14:00:00-05:00'
    },
    {
      start: '2017-02-21T16:00:00-05:00',
      end: '2017-02-21T18:00:00-05:00'
    }
  ]);
});
