import moment from 'moment';

const sortEvents = events =>
  events.sort((a, b) => {
    if (moment(a.start).unix() <= moment(b.start).unix()) {
      return -1;
    }
    if (moment(a.start).unix() > moment(b.start).unix()) {
      return 1;
    }
    return 0;
  });

const cleanEvents = events => {
  const cleanedEvents = events;

  events.map((event, i, eventList) => {
    const previousEvent = eventList[i - 1];
    const nextEvent = eventList[i + 1];

    if (nextEvent) {
      if (moment(event.start).isSameOrBefore(nextEvent.start)) {
        if (moment(event.end).isBetween(nextEvent.start, nextEvent.end)) {
          cleanedEvents.splice(i, 1);
        } else if (moment(nextEvent.end).isBetween(event.start, event.end)) {
          cleanedEvents.splice(i + 1, 1);
        }
      }
    }
    if (previousEvent) {
      if (moment(event.start).isSameOrAfter(previousEvent.start)) {
        if (
          moment(event.end).isBetween(previousEvent.start, previousEvent.end)
        ) {
          cleanedEvents.splice(i, 1);
        } else if (
          moment(previousEvent.end).isBetween(event.start, event.end)
        ) {
          cleanedEvents.splice(i - 1, 1);
        }
      }
    }
  });

  return cleanedEvents;
};

const findFreeTimes = (start, end, duration, events) => {
  const freeTimes = [];

  const cleanedSortedEvents = cleanEvents(sortEvents(events));

  let counter = start;
  cleanedSortedEvents.map(event => {
    const freeTime = {
      start: counter.format()
    };

    while (
      moment(counter)
        .add(duration, 'm')
        .isSameOrBefore(event.start)
    ) {
      counter = moment(event.start);
    }

    freeTime.end = counter.format();
    if (!moment(freeTime.start).isSame(freeTime.end)) {
      freeTimes.push(freeTime);
    }

    counter = moment(event.end);
  });

  if (counter.isBefore(end)) {
    freeTimes.push({
      start: counter.format(),
      end: end.format()
    });
  }

  return freeTimes;
};

export default findFreeTimes;
