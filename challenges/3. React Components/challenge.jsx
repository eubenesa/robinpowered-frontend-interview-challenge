import { cx } from 'emotion';
import moment from 'moment';
import React from 'react';
import PropTypes from 'prop-types';
import uuidv4 from 'uuid/v4';

import findFreeTimes from '../1. Meeting Availability/challenge';

const Pill = ({ className, children }) => (
  <div className={cx('pill', className)}>{children}</div>
);

Pill.propTypes = {
  className: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.arrayOf(PropTypes.string)
  ]),
  children: PropTypes.string
};

Pill.defaultProps = {
  className: '',
  children: moment().format('hh:mma')
};

const TimeAvailabilityPills = props => {
  const { start, end, duration, events } = props;

  const renderPills = () => {
    const pills = [];
    const freeTimes = findFreeTimes(start, end, duration, events);

    let counter = moment(start);
    while (
      moment(counter)
        .add(duration, 'm')
        .isSameOrBefore(end)
    ) {
      if (
        counter.isSameOrBefore(freeTimes[0].start) ||
        counter.isSameOrAfter(freeTimes[freeTimes.length - 1].end)
      ) {
        pills.push(
          <Pill className="available" key={uuidv4()}>
            {counter.format('hh:mma')}
          </Pill>
        );
      } else {
        const availability = freeTimes.reduce((accumulator, freeTime) => {
          if (
            counter.isSameOrAfter(freeTime.start) &&
            counter.isBefore(freeTime.end)
          ) {
            return 'available';
          }
          return accumulator;
        }, 'unavailable');

        pills.push(
          <Pill className={availability} key={uuidv4()}>
            {counter.format('hh:mma')}
          </Pill>
        );
      }
      counter = counter.add(duration, 'm');
    }

    return pills;
  };

  return <div className="time-availability-pills">{renderPills()}</div>;
};

TimeAvailabilityPills.propTypes = {
  start: PropTypes.instanceOf(moment),
  end: PropTypes.instanceOf(moment),
  duration: PropTypes.number,
  events: PropTypes.arrayOf(PropTypes.object)
};

TimeAvailabilityPills.defaultProps = {
  start: moment().format('hh:mma'),
  end: moment().format('hh:mma'),
  duration: 0,
  events: []
};

export { Pill, TimeAvailabilityPills };
