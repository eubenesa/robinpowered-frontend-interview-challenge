import { shallow } from 'enzyme'; // eslint-disable-line import/no-extraneous-dependencies
import moment from 'moment';
import React from 'react';

import { TimeAvailabilityPills } from './challenge.jsx';

const start = moment('2018-11-13T08:00:00-05:00');
const end = moment('2018-11-13T11:00:00-05:00');

const events = [
  {
    title: 'Meeting A',
    start: '2018-11-13T09:00:00-05:00',
    end: '2018-11-13T10:00:00-05:00'
  }
];

test('React Components', () => {
  const wrapper = shallow(
    <TimeAvailabilityPills
      start={start}
      end={end}
      duration={30}
      events={events}
    />
  );
  expect(wrapper.html()).toBe(
    '<div class="time-availability-pills"><div class="pill available">08:00am</div><div class="pill available">08:30am</div><div class="pill unavailable">09:00am</div><div class="pill unavailable">09:30am</div><div class="pill available">10:00am</div><div class="pill available">10:30am</div></div>'
  );
});
