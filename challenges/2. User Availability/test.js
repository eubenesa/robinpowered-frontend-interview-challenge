import moment from 'moment';

import findCommonFreeTimes from './challenge';
import userScheduleA from './scheduleA.json';
import userScheduleB from './scheduleB.json';

const start = moment('2017-02-21T08:00:00-05:00');
const end = moment('2017-02-21T18:00:00-05:00');

const commonFreeTimes = findCommonFreeTimes(
  [userScheduleA, userScheduleB],
  start,
  end,
  30
);

test('User Availability', () => {
  expect(commonFreeTimes).toEqual([
    {
      start: '2017-02-21T08:00:00-05:00',
      end: '2017-02-21T09:00:00-05:00'
    },
    {
      start: '2017-02-21T10:15:00-05:00',
      end: '2017-02-21T11:00:00-05:00'
    },
    {
      start: '2017-02-21T15:30:00-05:00',
      end: '2017-02-21T18:00:00-05:00'
    }
  ]);
});
