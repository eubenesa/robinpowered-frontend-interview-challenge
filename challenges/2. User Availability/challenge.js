import findFreeTimes from '../1. Meeting Availability/challenge';

const findCommonFreeTimes = (userSchedules, after, before, duration) => {
  const events = [];

  userSchedules.map(schedule => events.push(...schedule));

  return findFreeTimes(after, before, duration, events);
};

export default findCommonFreeTimes;
