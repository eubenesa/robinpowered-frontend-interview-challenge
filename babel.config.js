module.exports = {
  presets: [
    [
      "@babel/preset-env",
      {
        loose: true,
        modules: false,
        useBuiltIns: "usage"
      }
    ],
    [
      "@babel/preset-react",
      {
        useBuiltIns: true
      }
    ]
  ],
  env: {
    test: {
      presets: ["@babel/preset-env", "@babel/preset-react"]
    }
  }
};
